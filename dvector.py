import os
import keras
import numpy as np
from os.path import exists
from keras import callbacks
from keras.models import Model
from argparse import ArgumentParser
from sklearn.preprocessing import LabelEncoder
from capsulelayers import CapsuleLayer, PrimaryCap
from keras.utils import multi_gpu_model,to_categorical
from keras.layers import Dense, Conv2D, Input, Flatten


def getLabels(path):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    return labels, label_indices

def architecture(nClass):
    x = Input(shape=(40, 188, 1))

    conv1 = Conv2D(filters=256, kernel_size=(1, 1), strides=1, padding='valid', activation='relu', name='conv1')(x)
    primarycaps = PrimaryCap(conv1, dim_capsule=8, n_channels=16, kernel_size=(1, 1), strides=1, padding='valid',
                             name="primarycaps_conv2d")
    classcaps = CapsuleLayer(num_capsule=nClass, dim_capsule=8, routings=2, name='classcaps')(primarycaps)
    out_caps = Flatten(name='capsnet')(classcaps)
    fc6 = Dense(2048, activation='relu', input_dim=16 * nClass)(out_caps)
    fc7 = Dense(1024, activation='relu')(fc6)
    out = Dense(nClass, activation='softmax')(fc7)
    model = Model(inputs=x, outputs=out)

    model.compile(loss='categorical_crossentropy',
                  optimizer=keras.optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0),
                  metrics=['accuracy'])

    model.summary()
    return model

class NeuralNet:
    X = None
    Y = None
    model = None

    def __init__(self, model):
        self.model = model

    def loadDataset(self, src, labels):
        self.X = np.load(src + labels[0] + '.npy')
        self.Y = np.load(src + "classes-" + labels[0] + '.npy')

        for i, label in enumerate(labels[1:]):
            x = np.load(src + label + '.npy')
            y = np.load(src + "classes-" + label + '.npy')
            self.X = np.vstack((self.X, x))
            self.Y = np.concatenate((self.Y, y))

        encoder = LabelEncoder()
        self.Y = encoder.fit_transform(self.Y)

        assert self.X.shape[0] == len(self.Y)
        self._reshape()


    def fit(self, batch_size, epochs, verbose):
        self.model.fit(self.X, self.Y, batch_size=batch_size, epochs=epochs, verbose=verbose)

    def _reshape(self):
        N, mel, n = self.X.shape
        self.X = self.X.reshape((N, mel, n, 1))
        self.Y = to_categorical(self.Y)

    def makeDir(self,path):
        if not exists(path):
            os.makedirs(path)

    def saveModel(self,  destination):
        self.makeDir(destination)
        self.model.save(destination + 'embedding_last_epoch.hdf5')

if __name__ == '__main__':
    path = "VCTK-Corpus/wav48/"
    source = "MFCC/"
    destination =  "embed_model/"
    labels, _ = getLabels(path)

    NN = NeuralNet(architecture(len(labels)))
    NN.loadDataset(src = source,labels = labels)
    NN.fit(batch_size=8, epochs=10, verbose=1)
    NN.saveModel(destination)
import numpy as np
import numpy.random as rand
import os
import keras
import tensorflow as tf
from os.path import exists
from keras import callbacks
from keras import backend as K
from keras.models import Model
from argparse import ArgumentParser
from keras.utils import multi_gpu_model
from capsulelayers import CapsuleLayer, PrimaryCap
from keras.layers import Dense, Conv2D, Input, Flatten, Lambda
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve
from scipy.optimize import brentq
from scipy.interpolate import interp1d
from pyeer.eer_info import get_eer_stats

# python -m speaker_verification_model --data-dir MFCC/ --save-dir models/ --iter 1000 --batchsize 300

def arg_parse():
    arg_p = ArgumentParser()
    arg_p.add_argument('--data-dir', required=True) #mfcc's directory
    arg_p.add_argument('--save-dir', required=True) #directory to save the model
    arg_p.add_argument('--num-gpu', default=1, type=int) #number of gpu
    arg_p.add_argument('--batchsize', default=32, type=int) #number of batch
    arg_p.add_argument('--iter', default=10, type=int) #number of epoch
    return arg_p.parse_args()


def  loadDataset(args):
    def getTrain(args, labels):
        X = []
        x = np.load(args.data_dir + labels[0] + '.npy')
        X.append(x)
        for (i, label) in enumerate(labels[1:]):
            x = np.load(args.data_dir + label + '.npy')
            X.append(x)
        return X
    def getLabels(path):
        labels = os.listdir(path)
        label_indices = np.arange(0, len(labels))
        num_class = len(labels)
        return labels, label_indices, num_class

    # main
    labels, indices, num_class = getLabels("VCTK-Corpus/wav48")
    labels = np.sort(labels)
    X_train  = getTrain(args, labels)
    return X_train

def siameseModel():
    def speakerModel(input):
        conv1 = Conv2D(filters=256, kernel_size=(1, 1), strides=1, padding='valid', activation='relu', name='conv1_l')(
            input)
        primarycaps = PrimaryCap(conv1, dim_capsule=8, n_channels=16, kernel_size=(1, 1), strides=1, padding='valid',
                                 name="primarycap_conv2d_l")
        classcaps = CapsuleLayer(num_capsule=2, dim_capsule=8, routings=2,
                                 name='classcaps_l')(primarycaps)
        out_caps = Flatten(name='capsnet_l')(classcaps)
        fc6 = Dense(2048, activation='relu', input_dim=16 * 2)(out_caps)
        out = Dense(1024, activation='relu')(fc6)

        return out
    def embedModel(input):
        x = input
        num_label = 3 #sorry to say this, but you should change it manually
        conv1 = Conv2D(filters=256, kernel_size=(1, 1), strides=1, padding='valid', activation='relu', name='conv1')(x)
        primarycaps = PrimaryCap(conv1, dim_capsule=8, n_channels=16, kernel_size=(1, 1), strides=1, padding='valid',
                                 name='primarycap_conv2d')
        classcaps = CapsuleLayer(num_capsule=num_label, dim_capsule=8, routings=2,
                                 name='classcaps')(primarycaps)

        out_caps = Flatten(name='capsnet')(classcaps)
        fc6 = Dense(2048, activation='relu', input_dim=16 * num_label)(out_caps)
        fc7 = Dense(1024, activation='relu')(fc6)
        out = Dense(num_label, activation='softmax')(fc7)
        model = Model(inputs=x, outputs=out)

        model.load_weights('embed_model/embedding_last_epoch.hdf5')
        for layer in model.layers[:]:
            layer.trainable = False

        out = model.layers[-2].output

        return out

    x_l = Input(shape=(40, 188, 1))
    x_r = Input(shape=(40, 188, 1))

    embedOut = embedModel(input=x_r)
    speakerOut = speakerModel(input=x_l)
    L1_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    L1_distance = L1_layer([embedOut,speakerOut])
    prediction = Dense(1, activation='sigmoid')(L1_distance)
    siamese_net = Model(inputs=[x_l, x_r], outputs=prediction)
    siamese_net.compile(loss='binary_crossentropy',
                        optimizer=keras.optimizers.Adagrad(lr=0.001, epsilon=None, decay=0.0),
                        metrics=['accuracy'])

    siamese_net.summary()
    return siamese_net

def getBatch(args,X):
    N, mel, f = X[0].shape
    x_batch = [np.zeros((args.batchsize,mel,f)) for _ in range(2)]
    y_batch = np.zeros(args.batchsize)

    networkA = rand.choice(N, size=(args.batchsize,), replace=False)
    networkB = rand.choice(N, size=(args.batchsize,), replace=False)
    for i in range(args.batchsize):
        x_batch[0][i] = X[0][networkA[i]]
        if i <= args.batchsize//2:
            x_batch[1][i] = X[0][networkB[i]]
            y_batch[i] = 1
        else:
            x_batch[1][i] = X[rand.randint(1, len(X)-1)][networkB[i]]
            y_batch[i] = 0

    x_batch[0] = x_batch[0].reshape(args.batchsize, mel, f, 1)
    x_batch[1] = x_batch[1].reshape(args.batchsize, mel, f, 1)

    return x_batch, y_batch

def getTest(X_test, N, samples=100):
    _, mel, f = X[0].shape
    x = [np.zeros((N*samples, mel, f)) for _ in range(2)]
    y = np.zeros(shape=(N*samples))
    for i in range(0, len(y)-1,N):
        if i<samples//5:
            sample_a = rand.choice(len(X_test[0]), replace=False)
            for n in range(N):
                sample_b = rand.choice(len(X_test[0]), replace=False)
                x[0][i+n] = X_test[0][sample_a]
                x[1][i+n] = X_test[0][sample_b ]
                y[i+n] = 1
        else:
            falseCategotry = rand.choice(len(X_test[1:]), replace=False)
            sample_a = rand.choice(len(X_test[0]), replace=False)
            for n in range(N):
                sample_b = rand.choice(len(X_test[falseCategotry]), replace=False)
                x[0][i+n] = X_test[0][sample_a]
                x[1][i+n] = X_test[falseCategotry][sample_b]
                y[i+n] = 0

    x[0] = x[0].reshape(N*samples, mel, f, 1)
    x[1] = x[1].reshape(N*samples, mel, f, 1)

    return x,y

def splitData(X, random_state=12, test_size=0.1):
    X_train = []
    X_val = []
    for i in range(len(X)):
        x1,x2 = train_test_split(X[i], random_state=random_state, test_size=test_size)
        X_train.append(x1)
        X_val.append(x2)

    return X_train, X_val

def compute_roc_EER(fpr, tpr):
    roc_EER = []
    cords = zip(fpr, tpr)
    for item in cords:
        item_fpr, item_tpr = item
        if item_tpr + item_fpr == 1.0:
            roc_EER.append((item_fpr, item_tpr))
    assert(len(roc_EER) == 1.0)
    return np.array(roc_EER)

if __name__ == '__main__':
    # parse argument
    args = arg_parse()
    # load_dataset
    X = loadDataset(args)
    # split dataset into train test
    X_train, X_val = splitData(X)
    # prepare for data test
    x_test, y_test = getTest(X_val, N=4, samples=100)
    # create verification model. nb. it's also including embedding model
    model = siameseModel()
    # for _ in iters
    for iter in range(1, args.iter+1):
        # get batch
        x_batch, y_batch = getBatch(args,X_train)
        # train on batch
        loss, acc = model.train_on_batch(x=x_batch,y=y_batch)
        print("iter-{} loss: {} acc: {}".format(iter,round(loss, 2), round(acc, 2)))
        # calculate EER
        if iter==args.iter or iter%10==0:
            # predict test data  with current model
            y_pred = model.predict(x_test)
            # classify into maximum probability
            y_pred = np.argmax(y_pred,axis=1)
            print(y_pred==y_test)
            # calculate eer
            fpr, tpr, threshold = roc_curve(y_test, y_pred, pos_label=1)
            eer = brentq(lambda x: 1. - x - interp1d(fpr, tpr)(x), 0., 1.)
            thresh = interp1d(fpr, threshold)(eer)
            # show eer and threshold
            print("Test EER: {} threshold: {}".format(eer, thresh))
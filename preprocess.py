import librosa
import numpy as np
from tqdm import tqdm
import os
from os.path import exists

def wav2mfcc(wave):
    max_pad_len = 188
    mfcc = librosa.feature.mfcc(wave, sr=16000, n_mfcc=40)
    pad_width = max_pad_len - mfcc.shape[1]
    mfcc = np.pad(mfcc, pad_width=((0, 0), (0, pad_width)), mode='constant')
    return mfcc

def get_labels(path):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    return labels, label_indices

def getMfcc(filepath):
    wav, sr = librosa.load(filepath, mono=True, sr=None, offset=0, duration=2)
    mfcc = wav2mfcc(wav)
    return mfcc

def saveFeature(destination,label,data,classes):
    if not exists(destination):
        os.makedirs(destination)
    np.save(destination + label + '.npy', data)
    np.save(destination + "classes-" + label + '.npy', classes)

if __name__ == '__main__':
    path = "VCTK-Corpus/wav48/"
    labels, _ = get_labels(path)
    save_Destination = "MFCC/"

    for label in labels:
        mfccFiles = []
        classes = []
        for root, subdir, files in os.walk(os.path.join(path, label)):
            for file in tqdm(files):
                mfcc = getMfcc(os.path.join(root, file))
                mfccFiles.append(mfcc)
                classes.append(label)

        saveFeature(save_Destination,label, mfccFiles, classes)